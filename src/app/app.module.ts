import { HttpInterceptorService } from './http-interceptor.service';
import { AuthService } from './auth.service';
import { CompetitionService } from './competition.service';
import { SubscribersService } from './subscribers.service';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SubscribersComponent } from './subscribers/subscribers.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ViewComponent } from './view/view.component';
import { ViewByIdComponent } from './view-by-id/view-by-id.component';
import { CompetitionComponent } from './competition/competition.component';

import { CompetitionViewComponent } from './competition-view/competition-view.component';
import { ViewCompetitionByIdComponent } from './view-competition-by-id/view-competition-by-id.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';


@NgModule({
  declarations: [
    AppComponent,
    SubscribersComponent,
    ViewComponent,
    ViewByIdComponent,
    CompetitionComponent,
    CompetitionViewComponent,
    ViewCompetitionByIdComponent,
    LoginComponent,
    RegisterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule

 
  ],
  providers: [SubscribersService,CompetitionService,AuthService,{provide:HTTP_INTERCEPTORS,
    useClass:HttpInterceptorService,
    multi:true},],
  bootstrap: [AppComponent]
})
export class AppModule { }
