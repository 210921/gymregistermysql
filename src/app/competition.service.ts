import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';


const httpOptions ={
  headers: new HttpHeaders({'Content-Type':'application/json'})
}

@Injectable({
  providedIn: 'root'
})
export class CompetitionService {

  constructor(private http:HttpClient) { }

readonly root='http://localhost:8080/competition';

getCompetition(){
  return this.http.get('http://localhost:8080/competition');
}
getCompetitionById(id:number){
  return this.http.get('http://localhost:8080/competition/'+id)
}
addCompetionById(competitor:any){
  return this.http.post('http://localhost:8080/competition',competitor,httpOptions);
}
deleteCompetionById(id:number){
  return this.http.delete('http://localhost:8080/competition/'+id);
}

updateCompetition(id:number, competition:any){
  return this.http.put('http://localhost:8080/competition/'+id,competition).subscribe();
}



}
