import { ActivatedRoute } from '@angular/router';
import { CompetitionService } from './../competition.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-competition-view',
  templateUrl: './competition-view.component.html',
  styleUrls: ['./competition-view.component.scss']
})
export class CompetitionViewComponent implements OnInit {

  public competition:any;


  constructor(private competitionService: CompetitionService, private route:ActivatedRoute) { 


  }


  ngOnInit(): void {
this.getCompetition();

  }

  getCompetition(){
    this.competitionService.getCompetition().subscribe(
      data=> {console.log(data) 
        this.competition=data},
      err=>console.error(err),
      ()=>console.log('competition loaded')
    );
  }

  deleteCompetitionById(id:number){
this.competitionService.deleteCompetionById(id).subscribe(
  data=>{console.log(data) ;
  this.ngOnInit();
}) }

updateCompetition(id:number,competitor:any){
  this.competitionService.updateCompetition(id,competitor);
}

}
