import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';


const httpOptions ={
  headers: new HttpHeaders({'Content-Type':'application/json'})
}

@Injectable({
  providedIn: 'root'
})
export class SubscribersService {
  

  constructor(private http:HttpClient) { }

  getSubscribers(){
    return this.http.get('http://localhost:8080/gym')

  }
  addSubscriber(sub:any){
    return this.http.post('http://localhost:8080/gym',sub,httpOptions);
  }

  getSubscriberById(id:number){
    return this.http.get('http://localhost:8080/gym/'+id);
  }

  deleteSubscriberById(id:number){
  return this.http.delete('http://localhost:8080/gym/' + id);
  
  }
  updateSubscriber(id:number,subscriber:any){
   return this.http.put('http://localhost:8080/gym/'+id,subscriber).subscribe();

  }
  



}


