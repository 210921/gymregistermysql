import { SubscribersService } from './../subscribers.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class ViewComponent implements OnInit {
  public subscribers:any;

  constructor(private subscribersService:SubscribersService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    this.getSubscribers();
    this.deleteSubscriberById(this.route.snapshot.params.id);
  }
  getSubscribers(){
    return this.subscribersService.getSubscribers().subscribe(
      data=>{this.subscribers=data},
      err=>console.error(err),
      ()=>console.log('subscriber loaded')
    );
  }

  deleteSubscriberById(id:number){
    this.subscribersService.deleteSubscriberById(id)
    .subscribe(
      data=>{console.log(data);
      this.ngOnInit();
    })

  }

  updateSubscriber(id:number,subscriber:any){
    this.subscribersService.updateSubscriber(id,subscriber);
  }

}
