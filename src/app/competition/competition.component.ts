import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { SubscribersService } from './../subscribers.service';
import { CompetitionService } from './../competition.service';
import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-competition',
  templateUrl: './competition.component.html',
  styleUrls: ['./competition.component.scss']
})
export class CompetitionComponent implements OnInit {
subscriberList:any;
competitionForm!:FormGroup;
  validMessage: string="";
  competitionRegistration:any;
  isAddMode:any;
  readonly:boolean =false;


  constructor(private competitionService:CompetitionService,private subscribersService:SubscribersService,private route:ActivatedRoute) {
    this.subscribersService.getSubscribers().subscribe(sub=>this.subscriberList=sub);

   }

  ngOnInit(): void {
    let id = this.route.snapshot.params.id;         
    this.isAddMode = !id;
    
    this.competitionForm= new FormGroup({
      competitor:new FormControl('',Validators.required),
      category:new FormControl('',Validators.required),
      kg:new FormControl('',Validators.required),
      age:new FormControl('',Validators.required),
      competitionDate:new FormControl('',Validators.required),

    });
    if(!this.isAddMode) {
      this.competitionService.getCompetitionById(id)
        .pipe(first())
        .subscribe(x => this.competitionForm.patchValue(x))
    }

  }

  submitRegistration(){
  let id =this.route.snapshot.params.id;
  if(id){
    this.competitionService.updateCompetition(id,this.competitionForm.value);
    this.validMessage="Competition updated";
  }
else if(this.competitionForm?.valid){
  this.competitionService.addCompetionById(this.competitionForm.value).subscribe();
  this.validMessage = "Your competitor has been registrated. Thank you!";
  }else{
    this.validMessage="The form isn't complete"
  }

}
  getCompetitionRegistration(id:number){
    this.competitionService.getCompetitionById(id).subscribe(
      data=>{this.competitionRegistration=data;
    },
    err=>console.error(err),
    ()=>console.log('concurent loaded'));

  }

}
