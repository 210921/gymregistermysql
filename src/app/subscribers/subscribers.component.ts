import { SubscribersService } from './../subscribers.service';
import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';





@Component({
  selector: 'app-subscribers',
  templateUrl: './subscribers.component.html',
  styleUrls: ['./subscribers.component.scss']
})
export class SubscribersComponent implements OnInit {

  subscribersList:any;

  subscribersForm = new FormGroup({
    firstname :new FormControl('',Validators.required),
    lastname:new FormControl('',Validators.required),
    age:new FormControl('',Validators.required),
    start:new FormControl('',Validators.required),
    end:new FormControl('',Validators.required),
    price:new FormControl('',Validators.required),
  });
  
  isAddMode: boolean=false;
  validMessage : string ="";
  public subscriberRegistration:any;

  constructor(private route: ActivatedRoute, private subscribersSerice:SubscribersService) { }

  ngOnInit(): void {
    let id = this.route.snapshot.params.id;   
    this.isAddMode= !id;
    

    if(!this.isAddMode) {
      
      this.subscribersSerice.getSubscriberById(id)
      .pipe(first())
      .subscribe(x => this.subscribersForm.patchValue(x))
} 

    
  }

  submitRegistration(){
    let id = this.route.snapshot.params.id;
  if(id){
  this.subscribersSerice.updateSubscriber(id,this.subscribersForm.value);
  this.validMessage ="Subscriber updated";

}

  else if(this.subscribersForm?.valid){
    this.subscribersForm?.valid
      this.validMessage = "Your subscriber registration has been submitted. Thank you!";
      this.subscribersSerice.addSubscriber(this.subscribersForm.value).subscribe();
        // _data => {
        //   this.subscribersForm?.reset();
        //   return true;
        // },
        // error =>{
        //   return Observable.throw (error);
          
          
        // }
      
    }else{ this.validMessage ="Please fill out the form before submitting";}
  }

  getSubscriberRegistration(id:number){
    this.subscribersSerice.getSubscriberById(id).subscribe(
           data=>{ this.subscriberRegistration = data;
      },
     err =>console.error(err),
      ()=>console.log('subscriber loaded')
    );
  }
  
  






}


