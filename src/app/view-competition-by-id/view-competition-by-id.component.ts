import { ActivatedRoute } from '@angular/router';
import { CompetitionService } from './../competition.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-view-competition-by-id',
  templateUrl: './view-competition-by-id.component.html',
  styleUrls: ['./view-competition-by-id.component.scss']
})
export class ViewCompetitionByIdComponent implements OnInit {

  public competitionRegistration:any;
  readonly:boolean=false;

  constructor(private competitionService:CompetitionService, private route:ActivatedRoute) { }

  ngOnInit(): void {
    if(this.route.snapshot.queryParams['id']){
      this.getCompetitionById(this.route.snapshot.params.id);
    }
    this.getCompetitionById(this.route.snapshot.params.id);
  }

  getCompetitionById(id:number){
    this.competitionService.getCompetitionById(id).subscribe(
data=>{this.competitionRegistration=data;
},
err=>console.error(err),
()=>console.log('competition loaded')
    );
  }

}
