import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewCompetitionByIdComponent } from './view-competition-by-id.component';

describe('ViewCompetitionByIdComponent', () => {
  let component: ViewCompetitionByIdComponent;
  let fixture: ComponentFixture<ViewCompetitionByIdComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewCompetitionByIdComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewCompetitionByIdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
