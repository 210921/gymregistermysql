import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  username: string = "";
  password: string = "";

  constructor(private authService:AuthService) { }

  ngOnInit(): void {
  }

  register(){
    console.log("register login");
    this.authService.register(this.username,this.password);
  }

}
