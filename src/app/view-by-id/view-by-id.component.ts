import { ActivatedRoute } from '@angular/router';
import { SubscribersService } from './../subscribers.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-view-by-id',
  templateUrl: './view-by-id.component.html',
  styleUrls: ['./view-by-id.component.scss']
})
export class ViewByIdComponent implements OnInit {

  public subscriberRegistration:any;
  constructor(private subscribersService:SubscribersService, private route:ActivatedRoute) { }

  ngOnInit(): void {
    this.getSubscriberRegistration(this.route.snapshot.params.id);
    
  }
  getSubscriberRegistration(id:number){
    this.subscribersService.getSubscriberById(id).subscribe(
      data=>{this.subscriberRegistration=data;
      },
      err=>console.error(err),
      ()=> console.log('subscriber loaded')
    )
  }
}
