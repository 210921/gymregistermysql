import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { ViewCompetitionByIdComponent } from './view-competition-by-id/view-competition-by-id.component';
import { ViewByIdComponent } from './view-by-id/view-by-id.component';
import { ViewComponent } from './view/view.component';
import { SubscribersComponent } from './subscribers/subscribers.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CompetitionComponent } from './competition/competition.component';
import { CompetitionViewComponent } from './competition-view/competition-view.component';
import { AuthService } from './auth.service';

const routes: Routes = [
  {
    path:'view/:id',
    component:ViewByIdComponent,
    canActivate: [AuthService]
  },
  
  {
    path:'view',
    component:ViewComponent,
    canActivate: [AuthService]
  },
  {
    path:'home/:id',
    component: SubscribersComponent,
    canActivate: [AuthService]
  },
  {
    path:'home',
    component: SubscribersComponent,
    canActivate: [AuthService]
  },
  
  { path:'register', 
    component:RegisterComponent,
    canActivate: [AuthService]
  },
  {path:'competition/view/:id',
  component:ViewCompetitionByIdComponent,
  canActivate: [AuthService]
},
 {
   path:'competition/view',
   component: CompetitionViewComponent,
   canActivate: [AuthService]
 },
  {path:'competition/:id',
  component:CompetitionComponent,
  canActivate: [AuthService]
  },
  {
    path:'competition',
    component:CompetitionComponent,
    canActivate: [AuthService]
  },
  { path:'login',
    component:LoginComponent
  }

 

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
